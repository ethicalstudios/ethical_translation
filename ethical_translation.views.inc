<?php

/*
 * Implements hook_views_query_alter();
 *
 * This alters views queries for file lists. It adds the file language to the query so only, for example, french documents are shown when visitors are viewing the french site. Note that file language is different to the standard Drupal language. It was added to indicate the language of the content of the file rather than the language of the metadata.
 */
function ethical_translation_views_query_alter(&$view, &$query){

  if($view->name == 'openethical_documents' || $view->name == 'openethical_image_gallery' || $view->name == 'openethical_video_gallery'){

    global $language;

    foreach ($query->where[1]['conditions'] as $id => $condition) {
      if(is_string($condition['field']) && $condition['field'] == 'field_data_field_oe_file_language.field_oe_file_language_value'){
        $query->where[1]['conditions'][$id]['value'] = array($language->language => $language->language, 'und' => 'und');
        $query->where[1]['conditions'][$id]['operator'] = 'IN';
      }
    }
  }
}
